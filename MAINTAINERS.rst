.. _maintainers:

=============
 Maintainers
=============

The maintainer tools and documentation is maintained by:

* Daniel Vetter <daniel.vetter@ffwll.ch>
* Jani Nikula <jani.nikula@intel.com>

See also the full and current list of `project members`_.

.. _project members: https://gitlab.freedesktop.org/drm/maintainer-tools/project_members
